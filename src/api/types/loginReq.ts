/**
 * 登录
 */
export interface loginReq {
    account: string;  //账号
    password: string;  //密码
    verifyCode: string;  //验证码
}
