import axios from "@/utils/http"
import { loginReq } from './types/loginReq'
import { registReq } from './types/registReq'
import { tokenStore } from '@/store/modules/user';
//登录
export const login = (params: loginReq) => {
    return axios({
        url: "/api/user/login",
        method: "post",
        params,
        config: {
            headers: {
                'token': tokenStore().token
            },
            timeout: 3000
        }
    })
}
//注册
export const regist = (params: registReq) => {
    return axios({
        url: "/api/user/regist",
        method: "post",
        params,
        config: {
            headers: {
                'token': tokenStore().token
            },
            timeout: 3000
        }
    })
}
//退出登录
export const outLogin = (params:Object) => {
    return axios({
        url: "/api/user/outLogin",
        method: "get",
        params,
        config: {
            headers: {
                'token': tokenStore().token
            },
            timeout: 3000
        }
    })
}