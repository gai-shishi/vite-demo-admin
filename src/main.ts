import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import Particles from 'particles.vue3';
import piniaPluginPersist from 'pinia-plugin-persist';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';


const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }
const pinia = createPinia()
pinia.use(piniaPluginPersist)
app.use(router)
app.use(pinia)
app.use(ElementPlus)
app.use(Particles)
app.mount('#app')